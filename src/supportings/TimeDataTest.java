package supportings;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TimeDataTest {
	TimeData t20000101;
	TimeData t20000101_000000;
	TimeData t20010101;
	TimeData t20020101;
	TimeData t20030101;
	TimeData t20040101;
	TimeData t20050101;
	TimeData t20060101;
	TimeData t20070101;
	TimeData t20080101;
	TimeData t20090101;
	TimeData t20100101;
	TimeData t21000101;
	TimeData t22000101;
	TimeData t23000101;
	TimeData t24000101;
	TimeData t25000101;
	TimeData t40000101;
	TimeData t40010101;
	TimeData t20000102;
	TimeData t20000201;
	TimeData t20000202;
	TimeData t20001231;
	TimeData t20000101_120000;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		t20000101 = new TimeData(2000, 1, 1);
		t20000101_000000 = new TimeData(2020, 1, 1, 0, 0, 0);
		t20010101 = new TimeData(2001, 1, 1);
		t20020101 = new TimeData(2002, 1, 1);
		t20030101 = new TimeData(2003, 1, 1);
		t20040101 = new TimeData(2004, 1, 1);
		t20050101 = new TimeData(2005, 1, 1);
		t20060101 = new TimeData(2006, 1, 1);
		t20070101 = new TimeData(2007, 1, 1);
		t20080101 = new TimeData(2008, 1, 1);
		t20090101 = new TimeData(2009, 1, 1);
		t20100101 = new TimeData(2010, 1, 1);
		t21000101 = new TimeData(2100, 1, 1);
		t22000101 = new TimeData(2200, 1, 1);
		t23000101 = new TimeData(2300, 1, 1);
		t24000101 = new TimeData(2400, 1, 1);
		t24000101 = new TimeData(2400, 1, 1);
		t25000101 = new TimeData(2500, 1, 1);
		t40000101 = new TimeData(4000, 1, 1);
		t40010101 = new TimeData(4001, 1, 1);
		t20000102 = new TimeData(2000, 1, 2);
		t20000201 = new TimeData(2000, 2, 1);
		t20000202 = new TimeData(2000, 2, 2);
		t20001231 = new TimeData(2000, 12, 31);
		t20000101_120000 = new TimeData(2000, 1, 1, 12, 0, 0);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetVarianceFromTime() {
		assertEquals(366, t20010101.getVarianceFromTime(t20000101), 0.0);
		assertEquals(365, t20020101.getVarianceFromTime(t20010101), 0.0);
		assertEquals(731, t20020101.getVarianceFromTime(t20000101), 0.0);
		assertEquals(730850, t40010101.getVarianceFromTime(t20000101), 0.0);
	}

	@Test
	public void testToNumber() {
		assertEquals(0, t20000101.toNumber(), 0.0);
		assertEquals(366, t20010101.toNumber(), 0.0);
		assertEquals(731, t20020101.toNumber(), 0.0);
		assertEquals(1096, t20030101.toNumber(), 0.0);
		assertEquals(1461, t20040101.toNumber(), 0.0);
		assertEquals(1827, t20050101.toNumber(), 0.0);
		assertEquals(2192, t20060101.toNumber(), 0.0);
		assertEquals(2557, t20070101.toNumber(), 0.0);
		assertEquals(2922, t20080101.toNumber(), 0.0);
		assertEquals(3288, t20090101.toNumber(), 0.0);
		assertEquals(3653, t20100101.toNumber(), 0.0);
		assertEquals(36525, t21000101.toNumber(), 0.0);
		assertEquals(73049, t22000101.toNumber(), 0.0);
		assertEquals(109573, t23000101.toNumber(), 0.0);
		assertEquals(146097, t24000101.toNumber(), 0.0);
		assertEquals(182622, t25000101.toNumber(), 0.0);
		assertEquals(730485, t40000101.toNumber(), 0.0);
		assertEquals(730850, t40010101.toNumber(), 0.0);
		assertEquals(1, t20000102.toNumber(), 0.0);
		assertEquals(31, t20000201.toNumber(), 0.0);
		assertEquals(32, t20000202.toNumber(), 0.0);
		assertEquals(365, t20001231.toNumber(), 0.0);
		assertEquals(0.5, t20000101_120000.toNumber(), 0.0);
	}

	@Test
	public void testFromNumber() {
		assertEquals(new TimeData(0).toNumber(), t20000101.toNumber(), 0.0);
		assertEquals(new TimeData(366).toNumber(), t20010101.toNumber(), 0.0);
		assertEquals(new TimeData(731).toNumber(), t20020101.toNumber(), 0.0);
		assertEquals(new TimeData(1096).toNumber(), t20030101.toNumber(), 0.0);
		assertEquals(new TimeData(1461).toNumber(), t20040101.toNumber(), 0.0);
		assertEquals(new TimeData(1827).toNumber(), t20050101.toNumber(), 0.0);
		assertEquals(new TimeData(2192).toNumber(), t20060101.toNumber(), 0.0);
		assertEquals(new TimeData(2557).toNumber(), t20070101.toNumber(), 0.0);
		assertEquals(new TimeData(2922).toNumber(), t20080101.toNumber(), 0.0);
		assertEquals(new TimeData(3288).toNumber(), t20090101.toNumber(), 0.0);
		assertEquals(new TimeData(3653).toNumber(), t20100101.toNumber(), 0.0);
		assertEquals(new TimeData(36525).toNumber(), t21000101.toNumber(), 0.0);
		assertEquals(new TimeData(73049).toNumber(), t22000101.toNumber(), 0.0);
		assertEquals(new TimeData(109573).toNumber(), t23000101.toNumber(), 0.0);
		assertEquals(new TimeData(146097).toNumber(), t24000101.toNumber(), 0.0);
		assertEquals(new TimeData(182622).toNumber(), t25000101.toNumber(), 0.0);
		assertEquals(new TimeData(730485).toNumber(), t40000101.toNumber(), 0.0);
		assertEquals(new TimeData(730850).toNumber(), t40010101.toNumber(), 0.0);
		assertEquals(new TimeData(1).toNumber(), t20000102.toNumber(), 0.0);
		assertEquals(new TimeData(31).toNumber(), t20000201.toNumber(), 0.0);
		assertEquals(new TimeData(32).toNumber(), t20000202.toNumber(), 0.0);
		assertEquals(new TimeData(365).toNumber(), t20001231.toNumber(), 0.0);
		assertEquals(new TimeData(0.5).toNumber(), t20000101_120000.toNumber(), 0.0);
		for (int quasiMonth = 1; quasiMonth < 13; quasiMonth++) {
			assertEquals(30 * quasiMonth, (new TimeData(30 * quasiMonth)).toNumber(), 0.0);
		}
	}

}
