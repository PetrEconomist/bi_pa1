package supportings;

public class TimeData {
	// 1.1.2000 0:00 = 0

	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private int second;

	private final int YEAR_ZERO = 2000;

	public TimeData(int year, int month, int day) {
		this(year, month, day, 0, 0, 0);
	}

	public TimeData(int year, int month, int day, int hour, int minute, int second) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	public TimeData(double time) {
		double remainsToInterpret = time;
		this.year = daysToYears(remainsToInterpret);
		remainsToInterpret -= (new TimeData(this.year, 0, 0)).toNumber();
		this.month = daysToMonths(remainsToInterpret, this.year);
		remainsToInterpret = time - (new TimeData(this.year, this.month, 0)).toNumber();
		this.day = (int) remainsToInterpret;
		remainsToInterpret -= this.day;
		remainsToInterpret *= 24;
		this.hour = (int) remainsToInterpret;
		remainsToInterpret -= this.hour;
		remainsToInterpret *= 60;
		this.minute = (int) remainsToInterpret;
		remainsToInterpret -= this.minute;
		remainsToInterpret *= 60;
		this.second = (int) remainsToInterpret;
	}

	private int daysToYears(double time) {
		int yearOut = 2000;
		double daysRemaining = time;
		while (daysRemaining > (365 + isLeapYear(yearOut))) {
			daysRemaining -= 365 + isLeapYear(yearOut);
			yearOut++;
		}
		return yearOut;
	}

	double getVarianceFromTime(TimeData t) {
		return this.toNumber() - t.toNumber();
	}

	public double toNumber() {
		return yearToDays() + monthsToDays() + day - 1 + (hour + (minute + second / (double) 60) / 60) / 24;
	}

	private int yearToDays() {
		int yearDays = 0;
		for (int y = YEAR_ZERO; y < year; y++) {
			yearDays += 365 + isLeapYear(y);
		}
		return yearDays;
	}

	private int monthsToDays() {
		int out = 0;
		if (month > 1)
			out += 31;
		if (month > 2)
			out += (28 + isLeapYear(year));
		if (month > 3)
			out += 31;
		if (month > 4)
			out += 30;
		if (month > 5)
			out += 31;
		if (month > 6)
			out += 30;
		if (month > 7)
			out += 31;
		if (month > 8)
			out += 31;
		if (month > 9)
			out += 30;
		if (month > 10)
			out += 31;
		if (month > 11)
			out += 30;
		return out;
	}

	private int daysToMonths(double days, int year) {
		double daysRemaining = days;

		if ((daysRemaining -= 31) < 0)
			return 1;
		if ((daysRemaining -= (28 + isLeapYear(year))) < 0)
			return 2;
		if ((daysRemaining -= 31) < 0)
			return 3;
		if ((daysRemaining -= 30) < 0)
			return 4;
		if ((daysRemaining -= 31) < 0)
			return 5;
		if ((daysRemaining -= 30) < 0)
			return 6;
		if ((daysRemaining -= 31) < 0)
			return 7;
		if ((daysRemaining -= 31) < 0)
			return 8;
		if ((daysRemaining -= 30) < 0)
			return 9;
		if ((daysRemaining -= 31) < 0)
			return 10;
		if ((daysRemaining -= 30) < 0)
			return 11;
		return 12;

	}

	private int isLeapYear(int year) {
		if (year % 4000 == 0)
			return 0;
		if (year % 400 == 0)
			return 1;
		if (year % 100 == 0)
			return 0;
		if (year % 4 == 0)
			return 1;
		return 0;
	}




}