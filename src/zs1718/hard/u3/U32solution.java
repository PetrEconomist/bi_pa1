package zs1718.hard.u3;

import supportings.TimeData;

public class U32solution {

	final TimeData firstFullMoon = new TimeData(2000, 7, 16, 13, 55, 12);
	final double fullMoonCycle = 29.53059027;

	int isFullMoon(int y, int m, int d) {
		TimeData t = new TimeData(y, m, d, 23, 59, 59);
		if ((int) getPrevFullMoon(t) == (int) t.toNumber())
			return 1;
		return 0;
	}

	double getPrevFullMoon(TimeData t) {
		double fullMoonCount = Math.floor((t.toNumber() - firstFullMoon.toNumber()) / fullMoonCycle);
		return firstFullMoon.toNumber() + fullMoonCount * fullMoonCycle;
	}

	double getPrevFullMoon(int y, int m, int d) {
		return getPrevFullMoon(new TimeData(y, m, d, 23, 59, 59));
	}

	double getNextFullMoon(TimeData t) {
		return getPrevFullMoon(t) + fullMoonCycle;
	}
	
	double getNextFullMoon(int y, int m, int d) {
		return getPrevFullMoon(y,m,d) + fullMoonCycle;
	}

}
