package zs1718.hard.u3;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import supportings.TimeData;

public class U32solutionTest {
	U32solution solver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		solver = new U32solution();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsFullMoon() {
		assertEquals(1, solver.isFullMoon(2000, 7, 16));
		assertEquals(0, solver.isFullMoon(2000, 7, 17));
		assertEquals(1, solver.isFullMoon(2000, 8, 15));
		assertEquals(1, solver.isFullMoon(2000, 6, 17));
		assertEquals(1, solver.isFullMoon(2036, 1, 13));
		assertEquals(1, solver.isFullMoon(2026, 4, 31));
		assertEquals(1, solver.isFullMoon(2037, 1, 1));
	}

	@Test
	public void prevFullMoon() {
		assertEquals(new TimeData(2000, 7, 16).toNumber(), (int)solver.getPrevFullMoon(2000, 7, 17), 0.0);
		assertEquals(new TimeData(2000, 7, 16).toNumber(), (int)solver.getPrevFullMoon(2000, 7, 16), 0.0);
		assertEquals(new TimeData(2000, 6, 17).toNumber(), (int)solver.getPrevFullMoon(2000, 6, 18), 0.0);
		assertEquals(new TimeData(20540, 1, 1).toNumber(), (int)solver.getPrevFullMoon(20540, 1, 9), 0.0);
	}
	
	@Test
	public void nextFullMoon() {
		assertEquals(new TimeData(2026, 5, 31).toNumber(), (int)solver.getNextFullMoon(2026, 5, 26), 0.0);
		assertEquals(new TimeData(2000, 6, 17).toNumber(), (int)solver.getNextFullMoon(2000, 5, 18), 0.0);
		//as opposed to below, where next full moon is the same date because by default we ask for next from 00:00 
		assertEquals(new TimeData(2000, 5, 18).toNumber(), (int)solver.getNextFullMoon(new TimeData(2000, 5, 18)), 0.0);
	}

}
